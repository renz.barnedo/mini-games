import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor() {}

  setBoardDisplay(board: any) {
    board = Object.keys(board).map((key: string) => ({
      label: this.toTitleCase(key),
      value: board[key],
    }));
    return board;
  }

  toTitleCase(string: string) {
    let words = string.split(/(?=[A-Z])/);
    words = words.map(
      (word: string) => word[0].toUpperCase() + word.substring(1)
    );
    const label = words.join(' ');
    return label;
  }

  createSound(type: string) {
    const doc = document;
    const audio: any = doc.createElement('audio');
    audio.src = `../../../assets/${type}.mp3`;
    audio.setAttribute('preload', 'auto');
    audio.setAttribute('controls', 'none');
    audio.style.display = 'none';
    doc.body.appendChild(audio);
    return audio;
  }
}
