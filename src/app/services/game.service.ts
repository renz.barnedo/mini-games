import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../utilities/dialog/dialog.component';
import { NotifierComponent } from '../utilities/notifier/notifier.component';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private dialog: MatDialog) {}

  openNotifier(data: any) {
    this.dialog.open(NotifierComponent, {
      data,
      disableClose: true,
    });
  }

  closeNotifier() {
    this.dialog.closeAll();
  }

  shuffle(array: any) {
    let currentIndex = array.length,
      temporaryValue,
      randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  getRandomIntegerBetween(minimum: number, maximum: number): number {
    return Math.floor(Math.random() * (maximum - minimum + 1) + minimum);
  }

  openDialog(data: any) {
    this.dialog.open(DialogComponent, {
      data,
    });
  }
}
