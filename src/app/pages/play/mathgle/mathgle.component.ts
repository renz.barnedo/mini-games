import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { Settings } from 'src/app/models/settings';
import { DataService } from 'src/app/services/data.service';
import { GameService } from 'src/app/services/game.service';

@Component({
  selector: 'app-mathgle',
  templateUrl: './mathgle.component.html',
  styleUrls: ['./mathgle.component.scss'],
})
export class MathgleComponent implements OnInit {
  constructor(
    private router: Router,
    private gameService: GameService,
    private dataService: DataService
  ) {}

  choiceButtons: any[] = [
    { label: 'Greater Than', symbol: '>', color: 'primary' },
    { label: 'Less Than', symbol: '<', color: 'warn' },
    { label: 'Equal To', symbol: '=', color: 'accent' },
  ];
  ngOnInit() {
    this.setGame();
  }

  settings: Settings = localStorage.settings
    ? JSON.parse(localStorage.settings)
    : null;
  questions: any[] = [];
  currentQuestion: any;
  setGame() {
    if (!this.settings) {
      this.goToHome();
      return;
    }
    if (localStorage.questions) {
      this.questions = JSON.parse(localStorage.questions);
      const [lastItem] = this.questions.slice(-1);
      if (
        this.settings.numberOfQuestions <= this.questions.length &&
        lastItem.userAnswer
      ) {
        this.endTheGame();
        return;
      }
      this.currentQuestion = lastItem;
      this.setTimer();
      return;
    }

    this.setNextQuestion();
    this.setTimer();
  }

  setNextQuestion() {
    const fractionOne = this.getRandomFraction();
    const fractionTwo = this.getRandomFraction();
    const question = { fractionOne, fractionTwo };
    this.currentQuestion = question;
  }

  getRandomFraction() {
    const fraction: any = {
      numerator: this.getRandomIntegerBetween(0, 10),
      denominator: this.getRandomIntegerBetween(1, 10),
    };
    return fraction;
  }

  getRandomIntegerBetween(minimum: number, maximum: number): number {
    const integer = this.gameService.getRandomIntegerBetween(minimum, maximum);
    return integer;
  }

  isPlaying: boolean = true;
  async setAnswer(userAnswer: string) {
    this.timer = 0;
    this.currentQuestion.userAnswer = userAnswer;
    const { fractionOne, fractionTwo } = this.currentQuestion;

    let { numerator, denominator } = fractionOne;
    fractionOne.quotient = this.getQuotient(numerator, denominator);
    numerator = fractionTwo.numerator;
    denominator = fractionTwo.denominator;
    fractionTwo.quotient = this.getQuotient(numerator, denominator);

    const questionData = { fractionOne, fractionTwo, userAnswer };
    this.currentQuestion.correct = this.checkAnswer(questionData);
    const isCorrect = this.currentQuestion.correct.userAnswerIs;

    this.questions.push(this.currentQuestion);
    localStorage.questions = JSON.stringify(this.questions);

    await this.openAnswerCheckerDialog({ isCorrect, userAnswer });
    this.closeAnswerCheckerDialog();

    if (this.questions.length >= this.settings.numberOfQuestions) {
      this.stopTimer();
      this.endTheGame();
      return;
    }

    this.setNextQuestion();
    this.resetTimerValue();
  }

  getQuotient(numerator: number, denominator: number) {
    const quotient: any = numerator / denominator;
    if (!quotient || quotient === 0) {
      return quotient;
    }
    const withTwoDecimals = quotient
      .toString()
      .match(/^-?\d+(?:\.\d{0,2})?/)[0];
    return +withTwoDecimals;
  }

  checkAnswer({ ...data }) {
    const { fractionOne, fractionTwo, userAnswer } = data;
    let correctAnswer: string = '=';
    if (fractionOne.quotient > fractionTwo.quotient) {
      correctAnswer = '>';
    }
    if (fractionOne.quotient < fractionTwo.quotient) {
      correctAnswer = '<';
    }
    return {
      symbol: correctAnswer,
      userAnswerIs: correctAnswer === userAnswer,
    };
  }

  board: any;
  endTheGame() {
    this.currentQuestion = undefined;
    this.board = JSON.parse(localStorage.board);
    this.board = this.dataService.setBoardDisplay(this.board);
    this.isPlaying = false;
  }

  answerCheckerDialogIsOpen: boolean = false;
  async openAnswerCheckerDialog(question: any) {
    this.answerCheckerDialogIsOpen = true;
    this.gameService.openNotifier(question);
    return this.timeout(3000);
  }

  closeAnswerCheckerDialog() {
    this.answerCheckerDialogIsOpen = false;
    this.gameService.closeNotifier();
  }

  timeout(milliseconds: number) {
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
  }

  timer: any;
  resetTimerValue(): void {
    this.timer = this.settings.timer;
  }

  timerSubscription: any;
  setTimer() {
    this.resetTimerValue();
    this.timer = localStorage.timer
      ? JSON.parse(localStorage.timer)
      : this.settings.timer;
    this.timerSubscription = timer(0, 1000).subscribe(() => {
      this.countTimerDown();
      if (this.timer < 1) {
        this.setTimeUp();
      }
      this.setLocalStorageTimer();
    });
  }

  setLocalStorageTimer() {
    localStorage.timer = JSON.stringify(this.timer);
  }

  countTimerDown() {
    this.timer--;
  }

  setTimeUp() {
    this.timer = 0;
    if (
      !this.answerCheckerDialogIsOpen &&
      this.questions.length < this.settings.numberOfQuestions
    ) {
      this.setAnswer('');
    }
  }

  stopTimer(): void {
    this.timerSubscription.unsubscribe();
  }

  shuffle(array: any) {
    array = this.gameService.shuffle(array);
    return array;
  }

  goToHome() {
    this.router.navigateByUrl('');
  }

  setButtonBackground(question: any, button: any) {
    const noAnswer: boolean =
      !question.userAnswer && button.symbol === question.correct.symbol;
    if (noAnswer) {
      return { background: 'yellow', color: '#000' };
    }
    const clickedButton = question.userAnswer === button.symbol;
    const answerIsCorrect = question.correct.userAnswerIs;
    if (clickedButton && !answerIsCorrect) {
      return { background: 'orange', color: '#fff' };
    }
    if (clickedButton && answerIsCorrect) {
      return { background: 'green', color: '#fff' };
    }
    if (!clickedButton && button.symbol === question.correct.symbol) {
      return { background: 'green', color: '#fff' };
    }
    return {};
  }

  numberToWords: any = [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
  ];
  convertNumberToWord(number: string) {
    return this.numberToWords[number];
  }
}
