import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MathgleComponent } from './mathgle.component';

describe('MathgleComponent', () => {
  let component: MathgleComponent;
  let fixture: ComponentFixture<MathgleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MathgleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MathgleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
