import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { Question } from 'src/app/models/question';
import { Settings } from 'src/app/models/settings';
import { DataService } from 'src/app/services/data.service';
import { GameService } from 'src/app/services/game.service';

@Component({
  selector: 'app-colorword',
  templateUrl: './colorword.component.html',
  styleUrls: ['./colorword.component.scss'],
})
export class ColorwordComponent implements OnInit {
  constructor(
    private router: Router,
    private gameService: GameService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.setGame();
  }

  settings: Settings = localStorage.settings
    ? JSON.parse(localStorage.settings)
    : null;
  questions: Question[] = [];
  currentQuestion: any;
  isPlaying: boolean = false;
  setGame(): void {
    if (!this.settings) {
      this.goToHome();
      return;
    }
    if (localStorage.questions) {
      this.questions = JSON.parse(localStorage.questions);
      if (localStorage.isPlaying) {
        this.isPlaying = JSON.parse(localStorage.isPlaying);
        if (!this.isPlaying) {
          this.endTheGame();
          return;
        }
      }
      const [lastItem] = this.questions.slice(-1);
      this.currentQuestion = lastItem;
      this.currentQuestionIndex = this.questions.length - 1;
      this.isPlaying = true;
      this.setTimer();
      return;
    }

    this.setNextQuestion();
    this.isPlaying = true;
    this.setTimer();
  }

  colors: string[] = ['red', 'yellow', 'blue', 'orange', 'green', 'violet'];
  booleans: boolean[] = [true, false];
  setNextQuestion() {
    this.currentQuestionIndex++;
    let randomInteger: number = this.getRandomIntegerBetween(0, 5);
    const randomColor: string = this.colors[randomInteger];
    const withoutRandomColor: string[] = this.colors.filter(
      (color: string) => color !== randomColor
    );

    randomInteger = this.getRandomIntegerBetween(0, 4);
    const randomColorWord: string = withoutRandomColor[randomInteger];

    randomInteger = this.getRandomIntegerBetween(0, 1);
    const askForWord = this.booleans[randomInteger];

    let choices = this.setColorWordChoices(randomColor, randomColorWord);
    choices = this.shuffle(choices);
    const question: Question = {
      color: randomColor,
      word: randomColorWord,
      askForWord,
      choices,
    };

    this.questions.push(question);
    this.currentQuestion = question;
    localStorage.questions = JSON.stringify(this.questions);
  }

  setColorWordChoices(color: string, word: string) {
    const options: any[] = [
      {
        color,
        word,
      },
      { word: color, color: word },
    ];

    let otherColors: string[] = this.colors.filter(
      (otherColor: string) => otherColor !== color && otherColor !== word
    );
    let randomInteger: number = this.getRandomIntegerBetween(0, 3);
    let randomColor: string = otherColors[randomInteger];
    const withoutRandomColor: string[] = otherColors.filter(
      (color: string) => color !== randomColor
    );

    randomInteger = this.getRandomIntegerBetween(0, 2);
    const randomColorWord: string = withoutRandomColor[randomInteger];

    options.push(
      {
        color: randomColor,
        word: randomColorWord,
      },
      {
        color: randomColorWord,
        word: randomColor,
      }
    );

    return options;
  }

  currentQuestionIndex: number = -1;
  async setAnswer(answer: string) {
    this.timer = 0;
    const currentQuestion = this.questions[this.currentQuestionIndex];
    currentQuestion.answer = answer;

    await this.openAnswerCheckerDialog(currentQuestion);
    this.closeAnswerCheckerDialog();

    if (this.questions.length >= this.settings.numberOfQuestions) {
      this.stopTimer();
      this.endTheGame();
      return;
    }

    this.setNextQuestion();
    this.resetTimerValue();
  }

  answerCheckerDialogIsOpen: boolean = false;
  async openAnswerCheckerDialog(question: Question) {
    this.answerCheckerDialogIsOpen = true;
    this.gameService.openNotifier(question);
    return this.timeout(3000);
  }

  closeAnswerCheckerDialog() {
    this.answerCheckerDialogIsOpen = false;
    this.gameService.closeNotifier();
  }

  timeout(milliseconds: number) {
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
  }

  board: any;
  endTheGame() {
    this.isPlaying = false;
    localStorage.isPlaying = this.isPlaying;
    this.currentQuestion = undefined;
    this.board = JSON.parse(localStorage.board);
    this.board = this.dataService.setBoardDisplay(this.board);
  }

  getRandomIntegerBetween(minimum: number, maximum: number): number {
    const integer = this.gameService.getRandomIntegerBetween(minimum, maximum);
    return integer;
  }

  timer: any;
  resetTimerValue(): void {
    this.timer = this.settings.timer;
  }

  timerSubscription: any;
  setTimer() {
    this.resetTimerValue();
    this.timer = localStorage.timer
      ? JSON.parse(localStorage.timer)
      : this.settings.timer;
    this.timerSubscription = timer(0, 1000).subscribe(() => {
      this.countTimerDown();
      if (this.timer < 1) {
        this.setTimeUp();
      }
      this.setLocalStorageTimer();
    });
  }

  setLocalStorageTimer() {
    localStorage.timer = JSON.stringify(this.timer);
  }

  countTimerDown() {
    this.timer--;
  }

  setTimeUp() {
    this.timer = 0;
    if (!this.answerCheckerDialogIsOpen) {
      this.setAnswer('');
    }
  }

  stopTimer(): void {
    this.timerSubscription.unsubscribe();
  }

  setWordBackground(color: string) {
    const dark = '1f1e1e';
    const light = 'e4d2d2';
    const mapping: any = {
      red: light,
      yellow: dark,
      blue: light,
      orange: dark,
      green: light,
      violet: dark,
    };
    let background = mapping[color];
    background = `#${background}`;
    return background;
  }

  shuffle(array: any) {
    array = this.gameService.shuffle(array);
    return array;
  }

  goToHome() {
    this.router.navigateByUrl('');
  }
}
