import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorwordComponent } from './colorword.component';

describe('ColorwordComponent', () => {
  let component: ColorwordComponent;
  let fixture: ComponentFixture<ColorwordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorwordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorwordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
