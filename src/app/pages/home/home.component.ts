import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GameService } from 'src/app/services/game.service';

interface Game {
  path: string;
  label: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  gameForm: FormGroup = this.fb.group({});
  games: Game[] = [
    { path: 'colorword', label: 'Color Word' },
    { path: 'mathgle', label: 'Math GLE' },
  ];
  timerOptions: number[] = [3, 5, 6, 7, 10];
  numberOfQuestions: number[] = [5, 10, 15, 20, 30, 50, 100];

  @ViewChild('dialogTemplate') dialogTemplate: any;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private gameService: GameService
  ) {}

  settings: any;
  ngOnInit(): void {
    try {
      this.setDeferredPrompt();
      this.settings = localStorage.settings
        ? JSON.parse(localStorage.settings)
        : '';
      localStorage.clear();
      this.setLocalStorageSettings();
      this.setGameForm();
    } catch (error) {
      this.gameService.openDialog({
        title: 'Error',
        content: 'Initialization failed! Please try again.',
      });
    }
  }

  setLocalStorageSettings(): void {
    localStorage.settings = '';
  }

  setGameForm(): void {
    const gamePath = this.settings?.gamePath || 'colorword';
    const controls = {
      gamePath: this.fb.control(gamePath, Validators.required),
      timer: this.fb.control(5, Validators.required),
      numberOfQuestions: this.fb.control(5, Validators.required),
    };
    this.gameForm = this.fb.group(controls);
  }

  onFormSubmit(): void {
    if (this.gameForm.status !== 'VALID') {
      this.openDialog('Error', 'Form is invalid.');
      return;
    }

    const settings = this.gameForm.value;
    localStorage.settings = JSON.stringify(settings);
    this.router.navigateByUrl(`play/${settings.gamePath}`);
  }

  openDialog(title: string, message: string): void {
    this.dialog.open(this.dialogTemplate, {
      width: '300px',
      data: {
        title,
        message,
      },
    });
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }

  appInstalled: boolean = false;
  setDeferredPrompt() {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      this.deferredPrompt = e;
    });
    window.addEventListener('appinstalled', (evt) => {
      this.appInstalled = true;
    });
    window.addEventListener('DOMContentLoaded', () => {
      let displayMode = 'browser tab';
      const navigatorApi: any = navigator;
      if (navigatorApi.standalone) {
        displayMode = 'standalone-ios';
      }
      if (window.matchMedia('(display-mode: standalone)').matches) {
        displayMode = 'standalone';
      }
      if (displayMode.indexOf('standalone') > -1) {
        localStorage.hasInstalledPwa = true;
        this.appInstalled = true;
      }
    });
  }

  deferredPrompt: any;
  installPwa() {
    if (!this.deferredPrompt) {
      this.gameService.openDialog({
        title: 'Success',
        content: 'App install success! Check your apps list.',
      });
      return;
    }
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then((choiceResult: any) => {
      if (choiceResult.outcome === 'accepted') {
        this.gameService.openDialog({
          title: 'Success',
          content: 'App install success! Check your apps list.',
        });
      } else {
        this.gameService.openDialog({
          title: 'Error',
          content: 'App Install failed! Please try again.',
        });
      }
    });
  }
}
