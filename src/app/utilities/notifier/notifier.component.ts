import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-notifier',
  templateUrl: './notifier.component.html',
  styleUrls: ['./notifier.component.scss'],
})
export class NotifierComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataService: DataService
  ) {}

  showNotifier: boolean = false;
  correctSound: any;
  wrongSound: any;
  ngOnInit(): void {
    const settings = JSON.parse(localStorage.settings);
    settings.gamePath === 'colorword'
      ? this.checkColorWordAnswer()
      : this.checkMathGleAnswer();
    this.showNotifier = true;
  }

  display: any = { message: '', board: [] };
  checkColorWordAnswer() {
    this.correctSound = this.dataService.createSound('correct');
    this.wrongSound = this.dataService.createSound('wrong');
    const { answer, askForWord, color, word } = this.data;
    if (!answer) {
      this.wrongSound.play();
      this.setDisplayForAnswer('');
      this.setBoard({ corrects: 0, incorrects: 0, noAnswers: 1 });
      return;
    }
    if ((askForWord && answer !== word) || (!askForWord && answer !== color)) {
      this.wrongSound.play();
      this.setDisplayForAnswer('incorrect');
      this.setBoard({ corrects: 0, incorrects: 1, noAnswers: 0 });
      return;
    }
    this.setBoard({ corrects: 1, incorrects: 0, noAnswers: 0 });
    this.correctSound.play();
    this.setDisplayForAnswer('correct');
  }

  checkMathGleAnswer() {
    this.correctSound = this.dataService.createSound('correct');
    this.wrongSound = this.dataService.createSound('wrong');
    const { userAnswer } = this.data;
    if (!userAnswer) {
      this.wrongSound.play();
      this.setDisplayForAnswer('');
      this.setBoard({ corrects: 0, incorrects: 0, noAnswers: 1 });
      return;
    }

    const { isCorrect } = this.data;
    if (isCorrect) {
      this.correctSound.play();
      this.setBoard({ corrects: 1, incorrects: 0, noAnswers: 0 });
      this.setDisplayForAnswer('correct');
      return;
    }

    this.wrongSound.play();
    this.setDisplayForAnswer('incorrect');
    this.setBoard({ corrects: 0, incorrects: 1, noAnswers: 0 });
  }

  setDisplayForAnswer(answerCode: string) {
    if (!answerCode) {
      this.display.message = 'You did not answer.';
      return;
    }
    this.display.message = 'Your answer is ';
    this.display.message += answerCode + '.';
  }

  setBoard({ ...scores }) {
    const questions: any = JSON.parse(localStorage.questions);
    let board = localStorage.board;
    const { corrects, incorrects, noAnswers } = scores;
    const total = questions.length;

    if (!board) {
      this.display.board = {
        grade: this.getGrade(corrects, total),
        total,
        corrects,
        incorrects,
        noAnswers,
      };
      this.setBoardDisplay(this.display.board);
      return;
    }

    board = JSON.parse(localStorage.board);
    board.corrects += corrects;
    board.incorrects += incorrects;
    board.noAnswers += noAnswers;
    board.total = total;
    board.grade = this.getGrade(board.corrects, board.total);

    this.setBoardDisplay(board);
  }

  setBoardDisplay(board: any) {
    this.display.board = this.dataService.setBoardDisplay(board);
    localStorage.board = JSON.stringify(board);
  }

  getGrade(corrects: number, total: number) {
    const base = corrects / total;
    const hundredBased = base * 100;
    const integerText = hundredBased.toFixed(0);
    const fullGradeText = integerText + '%';
    return fullGradeText;
  }
}
