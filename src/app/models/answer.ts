export interface Answer {
  color: string;
  word: string;
}
