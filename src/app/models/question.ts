import { Answer } from './answer';

export interface Question {
  color: string;
  word: string;
  askForWord: boolean;
  choices: Answer[];
  answer?: string;
}
