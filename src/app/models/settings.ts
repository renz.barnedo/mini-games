export interface Settings {
  gameId: number;
  timer: number;
  numberOfQuestions: number;
}
