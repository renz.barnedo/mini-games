import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './themes/material/material.module';
import { HomeComponent } from './pages/home/home.component';
import { PlayComponent } from './pages/play/play.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ToolbarComponent } from './utilities/toolbar/toolbar.component';
import { NotifierComponent } from './utilities/notifier/notifier.component';
import { ColorwordComponent } from './pages/play/colorword/colorword.component';
import { MathgleComponent } from './pages/play/mathgle/mathgle.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { DialogComponent } from './utilities/dialog/dialog.component';

@NgModule({
  declarations: [AppComponent, HomeComponent, PlayComponent, ToolbarComponent, NotifierComponent, ColorwordComponent, MathgleComponent, DialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
